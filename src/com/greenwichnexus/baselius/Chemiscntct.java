package com.greenwichnexus.baselius;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class Chemiscntct extends Activity {
	
	
	ImageView chephone,chemail;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chemistry);
		chephone=(ImageView) findViewById(R.id.chemistry_phone);
		chephone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("tel:+914812202260"));
				startActivity(i);
				
				
			}
		});
		chemail=(ImageView) findViewById(R.id.chemistry_mail);
		chemail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);intent.setType("message/rfc822");
				intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"deptofchemistrybc@gmail.com"});
				Intent mailer = Intent.createChooser(intent, null);
				startActivity(mailer);
				
			}
		});
		
	}

}
