package com.greenwichnexus.baselius;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnShowListener;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;


public class UgPg extends ListActivity{

	// Progress Dialog
 //   private ProgressDialog pDialog;
 
    
    // Declare Variables
    ListView listview;
    CustomListViewAdapter adapter;
    List<UgPgItem> list_ugpg;
    String ugORpg = "";
    ObjectInputStream in;
    
    
 	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		   
		setContentView(R.layout.ugpg);
		
		Intent intent = getIntent();
		ugORpg = intent.getStringExtra("ugORpg");
		
		if(ugORpg.equalsIgnoreCase("ug")){
			    TextView txtvw = (TextView) findViewById(R.id.headingabout);
				txtvw.setText("UG");
				txtvw.setTypeface(Typeface.SANS_SERIF);
				// txtvw.setTextSize(22);
				 txtvw.setTextColor(Color.parseColor("#ffffff"));
				 
			}else{
				TextView txtvw = (TextView) findViewById(R.id.headingabout);
				txtvw.setText("PG");
				txtvw.setTypeface(Typeface.SANS_SERIF);
				// txtvw.setTextSize(22);
				 txtvw.setTextColor(Color.parseColor("#ffffff"));
				 
			}
		
		
		
       	
        list_ugpg = new ArrayList<UgPgItem>();
          
			try {
				
				if(ugORpg.equalsIgnoreCase("ug")){
					Log.e("before get file ug", "ug");
					in = new ObjectInputStream(new FileInputStream(new File(new File(getCacheDir(),"")+"UGDataFile.srl")));
					Log.e("after get file ug", "ug");
					// 					TextView txtvw = (TextView) findViewById(R.id.headingabout);
//					txtvw.setText("UG");
//					txtvw.setTypeface(Typeface.SANS_SERIF);
//					 txtvw.setTextSize(22);
//					 txtvw.setTextColor(Color.parseColor("#ffffff"));
					 
				}else{
					Log.e("before get file ug", "ug");
					in = new ObjectInputStream(new FileInputStream(new File(new File(getCacheDir(),"")+"PGDataFile.srl")));
					Log.e("after get file ug", "ug");
					// 					TextView txtvw = (TextView) findViewById(R.id.headingabout);
//					txtvw.setText("PG");
//					txtvw.setTypeface(Typeface.SANS_SERIF);
//					 txtvw.setTextSize(22);
//					 txtvw.setTextColor(Color.parseColor("#ffffff"));
//					 
				}
				
				
				String jsonArray=(String) in.readObject();
				
				in.close();
			    
				//   JSONObject j=new JSONObject(jsonObject);
			    JSONArray jarray=new JSONArray(jsonArray);
			    
			        for(int k=0;k<jarray.length();k++){
			        	
			        	JSONObject c = jarray.getJSONObject(k);
	  				    UgPgItem ugpgitem = new UgPgItem();
	  				    
	  				    ugpgitem.setdept(c.getString("course_name"));
	  				    ugpgitem.setnofseats(c.getString("seats_available"));
	  				    ugpgitem.setoccupied(c.getString("seats_occupied"));
	  				    list_ugpg.add(ugpgitem);
			        }
			     			
			} catch (StreamCorruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} /*catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/ catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			 listview = (ListView) findViewById(android.R.id.list);
  			 adapter = new CustomListViewAdapter(UgPg.this, list_ugpg);
  			 
  			 listview.setAdapter(adapter); 	     
		
		new asynctask().execute();
		
		}

 	
   private class asynctask extends AsyncTask<Void, Void, Void> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
 
 
            return null;
        }
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

               
        }
    }   
  }
