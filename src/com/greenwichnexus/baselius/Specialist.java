package com.greenwichnexus.baselius;

public class Specialist {

	private String  day;
    private String  specialist_name;
    private String  specialization;
    private String  timing;
    private String  leave;
    private boolean isHeader;
    
 
    public String getSpecialization(){
    		return specialization;
    }
    
    public void setSpecialization(String p_specialization){
		this.specialization = p_specialization;
    }
    
    public String getSpecialistName(){
		return specialist_name;
    }
    
    public void setSpecialistName(String p_specialist_name){
		this.specialist_name = p_specialist_name; 
    }
    
    public String getDay(){
		return day;
    }
    
    public void setDay(String p_day){
		this.day = p_day; 
    }
    
    public String getTiming(){
		return timing;
    }
    
    public void setTiming(String p_timing){
		this.timing = p_timing; 
    }
    
    public String getLeave(){
		return leave;
    }
    
    public void setLeave(String p_leave){
		this.leave = p_leave; 
    }
    
    public boolean isHeader(){
		return isHeader;
    }
    
    public void setHeader(Boolean p_isHeader){
		this.isHeader = p_isHeader; 
    }
    

	
	
}
