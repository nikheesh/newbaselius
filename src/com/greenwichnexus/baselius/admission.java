package com.greenwichnexus.baselius;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

public class admission extends Activity{
	
	ImageView admnof,proce,admnstatus,admisnstatus,admsche;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admission); 
		final File file = new File(new File(getCacheDir(),"")+"cacheFile.srl");
		
		admnof=(ImageView) findViewById(R.id.admofic);
		admnof.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/admission_officers.html");
				startActivity(i);
				
			}
		});
		proce=(ImageView) findViewById(R.id.proc);
		proce.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/admission_proced.html");
				startActivity(i);
				
				
			}
		});
		
		admnstatus=(ImageView) findViewById(R.id.admstat);
		admnstatus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Admissionstatus.class);
				startActivity(i);
				
			}
		});
		admsche=(ImageView) findViewById(R.id.admsche);
		admsche.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(file.exists())      
					
				{
					Intent i=new Intent(getApplicationContext(),Webabout.class);
					i.putExtra("link","admissionsche");
					startActivity(i);		}
				else
				{
					 Toast.makeText(getApplicationContext(),"No Internet connection", Toast.LENGTH_LONG).show();
				       
				}
				
				
			}
		});
		
		
}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Data.APP_RUNNING=false;
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Data.APP_RUNNING=true;
	}}
