package com.greenwichnexus.baselius;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AchievementAdapter extends BaseAdapter {
	
	// Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<AddHeading> list_headbody = null;
    //private List<TripObject> arraylist;
 
    public  AchievementAdapter(Context context,List<AddHeading> list_add){
        
    	mContext = context;
        this.list_headbody = list_add;
        inflater = LayoutInflater.from(mContext);
        this.list_headbody = new ArrayList<AddHeading>();
        this.list_headbody.addAll(list_add);
    }
 
    public class ViewHolder {
    	
    	TextView heada;
        TextView bodya;
       // TextView occupied;
       // TextView timing;
    }
 
    @Override
    public int getCount() {
        return list_headbody.size();
    }
 
    @Override
    public AddHeading getItem(int position) {
        return list_headbody.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.itemview1, null);
            // Locate the TextViews in listview_item.xml
            holder.heada = (TextView) view.findViewById(R.id.headpattern);
            holder.bodya = (TextView) view.findViewById(R.id.bodypattern);
            
          //  holder.occupied = (TextView) view.findViewById(R.id.occupied);
           // holder.day = (TextView) view.findViewById(R.id.separator);
            
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.heada.setText(Html.fromHtml(list_headbody.get(position).getHead()));
        
      //  holder.heada.setBackgroundResource(R.drawable.purplebar);
        holder.heada.setTextColor(Color.WHITE);
        holder.heada.setTypeface(Typeface.SANS_SERIF);
     //  holder.heada.setTextSize(18);
       
       
        	holder.bodya.setText(Html.fromHtml(list_headbody.get(position).getBody()));
            holder.bodya.setTextColor(Color.parseColor("#601261"));
            holder.bodya.setTypeface(Typeface.SANS_SERIF);
        
        view.setOnClickListener(new OnClickListener() {
  
            @Override
            public void onClick(View arg0) {
            	
            }
        });
        	return view;
    }

}
