package com.greenwichnexus.baselius;

import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class Engcontact extends Activity {
	
	
	ImageView engphone ,engmail;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eng);
		engphone=(ImageView) findViewById(R.id.phone);
		engphone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("tel:+914812565917"));
				startActivity(i);
				
				
				
			}
		});
		
		engmail=(ImageView) findViewById(R.id.eng_mail);
		engmail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);intent.setType("message/rfc822");
				intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"englishdept12@gmail.com"});
				Intent mailer = Intent.createChooser(intent, null);
				startActivity(mailer);
				
			}
		});
	}
	

}
