package com.greenwichnexus.baselius;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

public class Acadamics extends Activity {
	
	ImageView exam,seminar,research;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acadamics);
		final File file = new File(new File(getCacheDir(),"")+"cacheFile.srl");
		
		
		exam=(ImageView) findViewById(R.id.examination_btn);
		exam.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				if(file.exists())      
					
				{
					
					Intent i=new Intent(getApplicationContext(),Webabout.class);
					i.putExtra("link","exam");
					startActivity(i);		}
				else
				{
					 Toast.makeText(getApplicationContext(),"Please check your internet connection!", Toast.LENGTH_LONG).show();
				       
				}
				
				
				
			}
		});
		seminar=(ImageView) findViewById(R.id.seminars_btn);
		seminar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				if(file.exists())      
					
				{
					
					Intent i=new Intent(getApplicationContext(),Webabout.class);
					i.putExtra("link","seminar");
					startActivity(i);		
					}
				else
				{
					 Toast.makeText(getApplicationContext(),"Please check your internet connection!", Toast.LENGTH_LONG).show();
				       
				}
				
				
			}
		});
		
		research=(ImageView) findViewById(R.id.research_btn);
		research.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				if(file.exists())      
					
				{
					
					Intent i=new Intent(getApplicationContext(),Webabout.class);
					i.putExtra("link","research");
					startActivity(i);
					
				}
				else
				{
					 Toast.makeText(getApplicationContext(),"Please check your internet connection!", Toast.LENGTH_LONG).show();
				       
				}
				
			}
		});
		
	}

}
