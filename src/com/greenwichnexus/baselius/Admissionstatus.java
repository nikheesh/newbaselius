package com.greenwichnexus.baselius;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

public class Admissionstatus extends Activity {
	
	
	ImageView ug,pg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admissionstatus);
		final File fileu = new File(new File(getCacheDir(),"")+"UGDataFile.srl");
		final File filep = new File(new File(getCacheDir(),"")+"PGDataFile.srl");
		
		ug=(ImageView) findViewById(R.id.ug);
		ug.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
					if(fileu.exists())      
					
				{
						Intent i = new Intent(getApplicationContext(), UgPg.class);
		    			i.putExtra("ugORpg","ug");
		    			startActivity(i);
		    		
				}
				else
				{
					 Toast.makeText(getApplicationContext(),"No Internet connection", Toast.LENGTH_LONG).show();
				       
				}
				
				
			}
		});
		
		pg=(ImageView) findViewById(R.id.pg);
		pg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(fileu.exists())      
				
			{
					Intent i = new Intent(getApplicationContext(), UgPg.class);
	    			i.putExtra("ugORpg","pg");
	    			startActivity(i);

	    		
			}
			else
			{
				 Toast.makeText(getApplicationContext(),"No Internet connection", Toast.LENGTH_LONG).show();
			       
			}
			}
		});
	}
}
