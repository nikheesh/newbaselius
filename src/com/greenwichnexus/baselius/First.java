package com.greenwichnexus.baselius;



import java.io.File;

import info.androidhive.imageslider.FullScreenViewActivity;
import location.BigknolLocationService;
import location.LocationMapper;

import com.arellomobile.android.push.BasePushMessageReceiver;
import com.arellomobile.android.push.PushManager;
import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;



import location.*;

import com.pushwoosh.support.v4.app.NotificationCompat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;



public class First extends Activity implements LocationMapper{
	
	
	private static final String APP_ID = "30BE9-39726";//pushwush appid
	private static final String SENDER_ID = "346777325552";//google project ID

	boolean broadcastPush = true;

	String s1="+914812563918";
	BigknolLocationService bigknol_api;
	

	ImageView dept,adm,manag,about,notice,achievements,place,cocari,locate,gallery,call,you,web,fb,twit,cntctus,facil,academis;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_updated); 
		bigknol_api=new BigknolLocationService(First.this);
		

		final File file = new File(new File(getCacheDir(),"")+"cacheFile.srl");
		
		
		dept=(ImageView) findViewById(R.id.dept);
		dept.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),department.class);
				startActivity(i);
			}
		});
		cocari=(ImageView) findViewById(R.id.cocu);
		cocari.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),cocari.class);
				startActivity(i);
			}
		});
		adm=(ImageView) findViewById(R.id.admission);
		adm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),admission.class);
				startActivity(i);
			}
		});
		place=(ImageView) findViewById(R.id.place);
		place.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),placement.class);
				startActivity(i);
			}
		});
		manag=(ImageView) findViewById(R.id.mana);
		manag.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),management.class);
				startActivity(i);
			}
		});
		facil=(ImageView) findViewById(R.id.facilities);
		facil.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/facilities.html");
				startActivity(i);
				
			}
		});
		achievements=(ImageView) findViewById(R.id.ach);
		achievements.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(file.exists())      
					
				{
					
					Intent i=new Intent(getApplicationContext(),Achievements.class);
					i.putExtra("link","achievement");
					startActivity(i);
				}
				else
				{
					 Toast.makeText(getApplicationContext(),"Please check your internet connection!", Toast.LENGTH_LONG).show();
				       
				}
				
		
			}
		});
		about=(ImageView) findViewById(R.id.abt);
		about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),about.class);
				startActivity(i);
			}
		});
		
		notice=(ImageView) findViewById(R.id.notice);
		notice.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				if(file.exists())      
					
				{
					
					Intent i=new Intent(getApplicationContext(),Webabout.class);
					i.putExtra("link","notice");
					startActivity(i);
				}
				else
				{
					 Toast.makeText(getApplicationContext(),"Please check your internet connection!", Toast.LENGTH_LONG).show();
				       
				}
				
				}
		});
		
		academis=(ImageView) findViewById(R.id.gall);
		academis.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			
			Intent i=new Intent(getApplicationContext(),Acadamics.class);
				startActivity(i);
				
			}
		});
		
		cntctus=(ImageView) findViewById(R.id.locat);
		cntctus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),Web.class);
				i.putExtra("id","file:///android_asset/contact.html");
				startActivity(i);
				
			}
		});
		
		
		
		call=(ImageView) findViewById(R.id.ivCall);
        call.setOnClickListener(new OnClickListener() {
			
        	@Override
			public void onClick(View v) {
				Intent i=new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("tel:"+s1));
				startActivity(i);
				
			}
		});
        you=(ImageView) findViewById(R.id.ivYoutube);
        you.setOnClickListener(new OnClickListener() {
			
        	@Override
    		public void onClick(View v) {
    			try
    			{
    				
    				Intent intent=null;     
    				try {
    				        intent =new Intent(Intent.ACTION_VIEW);
    				        intent.setPackage("com.google.android.youtube");
    				        intent.setData(Uri.parse("https://www.youtube.com/user/baseliuscollege"));
    				        startActivity(intent);
    				    } catch (ActivityNotFoundException e) {
    				    	Intent i = new Intent(getApplicationContext(), you.class);
    						i.putExtra("id","https://www.youtube.com/user/baseliuscollege");
    						startActivity(i);
    				    	
    				        /*intent = new Intent(Intent.ACTION_VIEW);
    				        intent.setData(Uri.parse("http://www.youtube.com/user/keralatourismdotorg"));
    				        startActivity(intent);*/
    				    }
    				
    			}catch(Exception e)
    			{    
    				
    				/*Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("http://youtube.com/keralatourismdotorg"));
    				startActivity(i);*/
    				
    				
    				
    				Intent i = new Intent(getApplicationContext(), you.class);
    				i.putExtra("id","https://www.youtube.com/user/baseliuscollege");
    				startActivity(i);
    				
    			}	
    			
    		}
		});
        web=(ImageView) findViewById(R.id.ivWeb);
        web.setOnClickListener(new OnClickListener() {
			
        	@Override
			public void onClick(View v) {
        		Intent i = new Intent(getApplicationContext(), you.class);
    			i.putExtra("id","http://www.baselius.ac.in/");
    			startActivity(i);
				
			}
		});
        fb=(ImageView) findViewById(R.id.ivFacebook);
        fb.setOnClickListener(new OnClickListener() {
			
       	 @Override
 		public void onClick(View v) {
 			try
 			{
 				Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("fb://profile/337561636370819"));
 				startActivity(i);

 			}catch(Exception e)
 			{    
 				
 				
 			    /*Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/KeralaTravelMart"));
 				startActivity(i);*/
 				Intent i = new Intent(getApplicationContext(), you.class);
 				i.putExtra("id","https://www.facebook.com/pages/Baselius-College-Kottayam/337561636370819");
 				startActivity(i);
 				
 			}	
 			
 		}
		});
        twit=(ImageView) findViewById(R.id.ivTwitter);
        twit.setOnClickListener(new OnClickListener() {
			
        	@Override
    		public void onClick(View v) {
    			try
    			{
    				Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("twitter://user?screen_name=baseliustweets"));
    				startActivity(i);

    			}catch(Exception e)
    			{    
    				
    				/*Intent i=new Intent(Intent.ACTION_VIEW,Uri.parse("https://twitter.com/KTMsociety"));
    				startActivity(i);*/
    				Intent i = new Intent(getApplicationContext(), you.class);
    				i.putExtra("id","https://twitter.com/baseliustweets");
    				startActivity(i);
    				
    			}
    			
    		}
		});
		gallery=(ImageView) findViewById(R.id.ivGallery);
		gallery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ConnectivityManager cn=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		        NetworkInfo nf=cn.getActiveNetworkInfo();
		        if(nf != null && nf.isConnected()==true )
		        {
				
				
				Intent intent=new Intent(First.this,FullScreenViewActivity.class);
				startActivity(intent);
				
			}
			
		        else
		        {  
		        	
		        	
		        	
		            Toast.makeText(getApplicationContext(), "You are offline", Toast.LENGTH_LONG).show();
		            
		            Toast.makeText(getApplicationContext(), "Please check the connection settings", Toast.LENGTH_LONG).show();
		            
		        }
				   
			}
		});
		
		locate=(ImageView) findViewById(R.id.locate);
		
		locate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			//  GPStask();   	
				   
				  try {
					  if(hasConnection())
					  {
						//  GPStask(); 
		        	networkTask();
					  }
					  else
					  {
						  Toast.makeText(getApplicationContext(), "No Internet Found!", Toast.LENGTH_SHORT).show();
						  
					  }
					 
					//  GPStask();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("Error"+e.getMessage());
					
				}
			}
		});
/////////pushwoosh///////////
		registerReceivers();

		//Create and start push manager
		PushManager pushManager = new PushManager(this, APP_ID, SENDER_ID);
		pushManager.onStartup(this);
		PushManager.setMultiNotificationMode(this);
		checkMessage(getIntent(),true);
/////////pushwoosh///////////		
	}
	/////////pushwoosh///////////
	/**
	 * Called when the activity receives a new intent.
	 */
	public void onNewIntent(Intent intent)
	{

		super.onNewIntent(intent);
	//	Toast.makeText(getApplicationContext(), "onNewIntent", 0).show();
		Log.i("onnewintent", "yeah");
		//have to check if we've got new intent as a part of push notification
		checkMessage(intent,true);
	}
	@Override
	public void onResume()
	{
		super.onResume();
		Data.APP_RUNNING=true;
		//Re-register receivers on resume
		registerReceivers();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		Data.APP_RUNNING=false;
		//Unregister receivers on pause
		unregisterReceivers();
	}

	//Push message receiver
	private BroadcastReceiver mReceiver = new BasePushMessageReceiver()
	{
		@Override
		protected void onMessageReceive(Intent intent)
		{
			//JSON_DATA_KEY contains JSON payload of push notification.
			//Toast.makeText(getApplicationContext(), "broadcastReciever:"+intent.getExtras().getString(JSON_DATA_KEY), 0).show();
			doOnMessageReceive(intent.getExtras().getString(JSON_DATA_KEY),false);
		}
	};

	NotificationCompat.Builder notification;
	PendingIntent pIntent;
	NotificationManager manager;
	Intent resultIntent;
	TaskStackBuilder stackBuilder;
	public void doOnMessageReceive(String message,boolean showNotification)
	{
		Toast.makeText(getApplicationContext(), "New Notification", 0).show();

		if(showNotification)
		{
			Intent intent= new Intent(this,Notification.class);
			intent.putExtra("json", message);
			Log.i("json", message);
			startActivity(intent);
		}
	}


	//Registration receiver
	BroadcastReceiver mBroadcastReceiver = new RegisterBroadcastReceiver()
	{
		@Override
		public void onRegisterActionReceive(Context context, Intent intent)
		{
			checkMessage(intent,false);
		}
	};

	private void checkMessage(Intent intent,boolean showNotification)
	{
		if (null != intent)
		{
			if (intent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
			{
				doOnMessageReceive(intent.getExtras().getString(PushManager.PUSH_RECEIVE_EVENT),showNotification);
			}
			/*else if (intent.hasExtra(PushManager.REGISTER_EVENT))
				{
					doOnRegistered(intent.getExtras().getString(PushManager.REGISTER_EVENT));
				}
				else if (intent.hasExtra(PushManager.UNREGISTER_EVENT))
				{
					doOnUnregisteredError(intent.getExtras().getString(PushManager.UNREGISTER_EVENT));
				}
				else if (intent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
				{
					doOnRegisteredError(intent.getExtras().getString(PushManager.REGISTER_ERROR_EVENT));
				}
				else if (intent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
				{
					doOnUnregistered(intent.getExtras().getString(PushManager.UNREGISTER_ERROR_EVENT));
				}*/

			resetIntentValues();
		}
	}

	private void resetIntentValues()
	{
		Intent mainAppIntent = getIntent();

		if (mainAppIntent.hasExtra(PushManager.PUSH_RECEIVE_EVENT))
		{
			mainAppIntent.removeExtra(PushManager.PUSH_RECEIVE_EVENT);
		}
		else if (mainAppIntent.hasExtra(PushManager.REGISTER_EVENT))
		{
			mainAppIntent.removeExtra(PushManager.REGISTER_EVENT);
		}
		else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_EVENT))
		{
			mainAppIntent.removeExtra(PushManager.UNREGISTER_EVENT);
		}
		else if (mainAppIntent.hasExtra(PushManager.REGISTER_ERROR_EVENT))
		{
			mainAppIntent.removeExtra(PushManager.REGISTER_ERROR_EVENT);
		}
		else if (mainAppIntent.hasExtra(PushManager.UNREGISTER_ERROR_EVENT))
		{
			mainAppIntent.removeExtra(PushManager.UNREGISTER_ERROR_EVENT);
		}

		setIntent(mainAppIntent);
	}
	public void registerReceivers()
	{
		IntentFilter intentFilter = new IntentFilter(getPackageName() + ".action.PUSH_MESSAGE_RECEIVE");

		if(broadcastPush)
			registerReceiver(mReceiver, intentFilter);

		registerReceiver(mBroadcastReceiver, new IntentFilter(getPackageName() + "." + PushManager.REGISTER_BROAD_CAST_ACTION));		
	}

	public void unregisterReceivers()
	{
		//Unregister receivers on pause
		try
		{
			unregisterReceiver(mReceiver);
		}
		catch (Exception e)
		{
			// pass.
		}

		try
		{
			unregisterReceiver(mBroadcastReceiver);
		}
		catch (Exception e)
		{
			//pass through
		}
	}
/////////pushwoosh///////////
////////location////////////	

	


	@Override
	public boolean hasConnection() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

	    NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	    if (wifiNetwork != null && wifiNetwork.isConnected()) {
	      return true;
	    }

	    NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	    if (mobileNetwork != null && mobileNetwork.isConnected()) {
	      return true;
	    }

	    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
	    if (activeNetwork != null && activeNetwork.isConnected()) {
	      return true;
	    }
		return false;
	}



	@Override
	public void MessageMe(String provider) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
    			First.this);

    	alertDialog.setTitle(provider + " SETTINGS");

    	alertDialog
    			.setMessage(provider + " is not enabled, Do you want to enable?");

    	alertDialog.setPositiveButton("Settings",
    			new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog, int which) {
    					Intent intent = new Intent(
    							Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    					First.this.startActivity(intent);
    				}
    			});

    	alertDialog.setNegativeButton("Cancel",
    			new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog, int which) {
    					dialog.cancel();
    				
    				}
    			});

    	alertDialog.show();


    }

    
  
    
   public void networkTask()
   {
   	
   	Location nwLocation = bigknol_api
   			.getLocation(LocationManager.NETWORK_PROVIDER);

   	if (nwLocation != null)
   	{
   		double latitude;
   		double longitude;
		
         
		try {
			latitude = nwLocation.getLatitude();
			longitude = nwLocation.getLongitude();
			Intent i = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://maps.google.com/maps?saddr=" + latitude
							+ "," + longitude + "&daddr=" + desti_lat + ","
							+ desti_lon));
			startActivity(i);
		} catch (Exception e) {
			// TODO: handle exception
			Log.d(TAG, error);
		}
   	
      
   		
   	}
   		else
   		{
   			MessageMe("Network");
   			
   		}
   		
   		
   	



   }
	}