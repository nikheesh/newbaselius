package com.greenwichnexus.baselius;

//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;


public class MainActivity extends Activity {
	private static int SPLASH_TIME_OUT=1500;
	private boolean isBackPressed = false;
   private JSONArray json;
   private JSONArray json1;
   private JSONArray json2;
   String jsonStr_ug;
   String jsonStr;
   String jsonStrPG;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	//	Fabric.with(this, new Crashlytics());
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        
       
        new GetSpinnerData().execute();
        new GetUGData().execute();
        new GetPGData().execute();
		
		new Handler().postDelayed(new Runnable(){
			public void run()
			{   if(!isBackPressed)
			{
				Intent i=new Intent(MainActivity.this,First.class);
				startActivity(i);
				finish();
			}}
		},SPLASH_TIME_OUT);
	}
	
	
	
	  @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	            isBackPressed = true;
	            finish();
	        }
	        return super.onKeyDown(keyCode, event);

	    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Data.APP_RUNNING=false;
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Data.APP_RUNNING=true;
	}
	
	 private class GetSpinnerData extends AsyncTask<Void, Void, Void> {
	   	 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	           
	            // Showing progress dialog
//	            pDialog = new ProgressDialog(MainActivity.this);
//	            pDialog.setMessage("Please wait...");
//	            pDialog.setCancelable(false);
//	            pDialog.show();
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	           Log.e("Inside doinbackgroumd  web ", "1");
	        	// Creating service handler class instance
	            ServiceHandler sh = new ServiceHandler();
	 
	            // Making a request to url and getting response
	            String url = "http://cochinfood.com/baselios/temp/getpagedata.php";
	        
	            
	            jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
	               if (jsonStr != null) {
	                try {

	                	 	 json=new JSONArray(jsonStr);
	                   //    	Log.e("json array is "," t h i   gg  "+json);
	                        JSONObject c = json.getJSONObject(1);
	                     //   Log.e("json array is "," t h i   gg  "+c);
	                      
	                      
	           
	                 
		                    
	                } catch (JSONException e) {
	                    e.printStackTrace();
	                } 
	            } else {
	                Log.e("ServiceHandler", "Couldn't get any data from the url for SPE");
	            }
	            
	           
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);   
	            Log.e("Inside postexe  web ", "1");
		        
	            ObjectOutput out;
				try {
					 if (jsonStr != null) {
					
						 out = new ObjectOutputStream(new FileOutputStream(new File(getCacheDir(),"")+"cacheFile.srl"));
						 Log.e("Inside before filewrite ", "1");
					      
						 out.writeObject(json.toString());
						 Log.e("Inside after filewrite ", "1");
						    
	                  out.close();
					 }
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				//	Crashlytics.logException(e);
				}
                //  out.writeObject(c.toString());
                  
	        }
	 }


	 private class GetUGData extends AsyncTask<Void, Void, Void> {
	   	 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	        	 Log.e("Inside doinbackgroumd  web ", "2");
	 	        
	        	// Creating service handler class instance
	            ServiceHandler sh = new ServiceHandler();
	 
	            // Making a request to url and getting response
	            String url_ug = "http://cochinfood.com/baselios/temp/getadmissionlist.php?coursetype=UG";
	            
	             jsonStr_ug = sh.makeServiceCall(url_ug, ServiceHandler.GET);
	         
	            if (jsonStr_ug != null) {
	                try {

	                	 	 json1=new JSONArray(jsonStr_ug);
	                   //    	Log.e("json array is "," t h i   gg  "+json);
	                        JSONObject c = json1.getJSONObject(1);
	                     //   Log.e("json array is "," t h i   gg  "+c);
	                       
	                        
	                } catch (JSONException e) {
	                    e.printStackTrace();
	                } 
	            } else {
	                Log.e("ServiceHandler", "Couldn't get any data from the url for SPE");
	            }
	            
	           
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);
	            Log.e("Inside postexe  web ", "2");
			      
	            // Toast.makeText(getApplicationContext(), "successfully saved ...!",10).show();
	            ObjectOutput out_ug;
				try { if (jsonStr_ug != null) {
					
					out_ug = new ObjectOutputStream(new FileOutputStream(new File(getCacheDir(),"")+"UGDataFile.srl"));
					 Log.e("Inside before filewrite ", "2");
					  
					out_ug.writeObject(json1.toString());
					 Log.e("Inside after filewrite ", "2");
					  
	                  out_ug.close();
				}
		          
				} 
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				//	Crashlytics.logException(e);
				}
                //  out.writeObject(c.toString());
                 
	            }
	 
	    }

	 
	 private class GetPGData extends AsyncTask<Void, Void, Void> {
	   	 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	     
	        }
	 
	        @Override
	        protected Void doInBackground(Void... arg0) {
	        	 Log.e("Inside doinbackgroumd  web ", "3");
	 	        
	        	// Creating service handler class instance
	            ServiceHandler sh = new ServiceHandler();
	 
	            // Making a request to url and getting response
	            String url = "http://cochinfood.com/baselios/temp/getadmissionlist.php?coursetype=PG";
	        
	            
	             jsonStrPG = sh.makeServiceCall(url, ServiceHandler.GET);
	         
	            if (jsonStrPG != null) {
	                try {

	                	  json2=new JSONArray(jsonStrPG);
	                   //    	Log.e("json array is "," t h i   gg  "+json);
	                        JSONObject c = json2.getJSONObject(1);
	                     //   Log.e("json array is "," t h i   gg  "+c);
	                                           
	                } catch (JSONException e) {
	                    e.printStackTrace();
	                } 
	            } else {
	                Log.e("ServiceHandler", "Couldn't get any data from the url for PG Data");
	            }
	            
	            return null;
	        }
	 
	        @Override
	        protected void onPostExecute(Void result) {
	            super.onPostExecute(result);
	            Log.e("Inside postexe  web ", "3");
			      
	           // Toast.makeText(getApplicationContext(), "successfully saved ...!",10).show();
	            ObjectOutput out;
				try {
					 if (jsonStrPG != null) {
				          
					out = new ObjectOutputStream(new FileOutputStream(new File(getCacheDir(),"")+"PGDataFile.srl"));
					 Log.e("Inside before filewrite ", "3");
					  
					out.writeObject(json2.toString());
					 Log.e("Inside before filewrite ", "3");
					  
	                  out.close();	}	
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Crashlytics.logException(e);
				}
                //  out.writeObject(c.toString());
                 
	         }
	 
	    }
	 
	 
	 
	
}
