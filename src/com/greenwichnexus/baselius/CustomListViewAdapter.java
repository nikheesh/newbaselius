package com.greenwichnexus.baselius;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class CustomListViewAdapter extends BaseAdapter {
	
	// Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<UgPgItem> list_ugpg = null;
    //private List<TripObject> arraylist;
 
    public CustomListViewAdapter(Context context,List<UgPgItem> list_specialists_availability){
        
    	mContext = context;
        this.list_ugpg = list_specialists_availability;
        inflater = LayoutInflater.from(mContext);
        this.list_ugpg = new ArrayList<UgPgItem>();
        this.list_ugpg.addAll(list_specialists_availability);
    }
 
    public class ViewHolder {
    	
    	TextView dept;
        TextView nofseats;
        TextView occupied;
       // TextView timing;
    }
 
    @Override
    public int getCount() {
        return list_ugpg.size();
    }
 
    @Override
    public UgPgItem getItem(int position) {
        return list_ugpg.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.itemugpg, null);
            // Locate the TextViews in listview_item.xml
            holder.dept = (TextView) view.findViewById(R.id.department);
            holder.nofseats = (TextView) view.findViewById(R.id.noofseats);
            holder.occupied = (TextView) view.findViewById(R.id.occupied);
           // holder.day = (TextView) view.findViewById(R.id.separator);
            
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.dept.setText(list_ugpg.get(position).getdept());
        holder.dept.setTextColor(Color.parseColor("#601261"));
        holder.dept.setTypeface(Typeface.SANS_SERIF);
       // holder.dept.setTextSize(getgetResources().getDimension(R.dimen.textSize12));
       
        holder.nofseats.setText(holder.nofseats.getText().toString()+list_ugpg.get(position).getnofseats());
        holder.nofseats.setTextColor(Color.parseColor("#601261"));
        holder.nofseats.setTypeface(Typeface.SANS_SERIF);
       
        holder.occupied.setText(holder.occupied.getText().toString()+list_ugpg.get(position).getoccupied());
        holder.occupied.setTextColor(Color.parseColor("#601261"));
        holder.occupied.setTypeface(Typeface.SANS_SERIF);
       
        // Set the results into TextViews
  /*      if(list_ugpg.get(position).getLeave()== null || list_ugpg.get(position).getLeave()=="" || list_ugpg.get(position).getLeave()== "null")
        	holder.spe.setText(list_ugpg.get(position).getSpecialistName());
        else{
        	Log.d("LEAVE TAG", list_ugpg.get(position).getLeave());
        	SpannableString text = new SpannableString(list_ugpg.get(position).getSpecialistName()+"( "+list_ugpg.get(position).getLeave()+" )"); 
        	int start = list_ugpg.get(position).getSpecialistName().length() + 3;
        	int end = start+ list_ugpg.get(position).getLeave().length();
        	text.setSpan(new ForegroundColorSpan(Color.RED), start - 1, end, 0);  
        	holder.spe.setText(text, BufferType.SPANNABLE);
        }
        
        
        holder.spe_class.setText(list_ugpg.get(position).getSpecialization());
        holder.timing.setText(list_ugpg.get(position).getTiming());
        holder.day.setText(list_ugpg.get(position).getDay());
        
        if(list_ugpg.get(position).getDay()==null)
        	holder.day.setVisibility(View.GONE);
        else
        	holder.day.setVisibility(View.VISIBLE);
        */
        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
            	
            }
        });
        	return view;
    }

}
