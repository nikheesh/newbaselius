package com.greenwichnexus.baselius;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class Webabout extends Activity{

	private String htmlcontent,heading;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webabout); 
		 ObjectInputStream in;
			try {
				
				
				in = new ObjectInputStream(new FileInputStream(new File(new File(getCacheDir(),"")+"cacheFile.srl")));
				//Log.d(" in   "," "+in); 
				//String jsonObject=(String) in.readObject();
				String jsonArray=(String) in.readObject();
				//JSONObject jsonObject = (JSONObject) in.readObject();
				//Log.d("JSONISHERE", jsonObject.toString());
				// htmlcontent=jsonObject.getString("pdesc");
			        in.close();
			     //   JSONObject j=new JSONObject(jsonObject);
			        JSONArray jarray=new JSONArray(jsonArray);
			    //    Log.d("jarray","jarray  is "+jarray);
			      //  Integer i1=Integer.parseInt(getIntent().getExtras().getString("index").toString());
			       String linkname= getIntent().getExtras().getString("link").toString();
			       
			        for( int k=0;k<jarray.length();k++){
			        	
			        	if((jarray.getJSONObject(k).getString("linkname").toString()).equals(linkname))
			        	{
			        		 
					        JSONObject j=jarray.getJSONObject(k);
					        
					        Log.d("jobject","jobject  is "+j);
					        heading=j.getString("ptitle");
					        htmlcontent=j.getString("pdesc");
					      //  String ab=getIntent().getExtras().getString("heading").toString();
							 TextView txt1 = (TextView)findViewById(R.id.headingabout);
							 txt1.setText(heading);
							txt1.setTypeface(Typeface.SANS_SERIF);
							// txt1.setTextSize(22);
							
							 txt1.setTextColor(Color.parseColor("#ffffff"));
							 
				
					        
				        TextView txt = (TextView)findViewById(R.id.htmltext);
			           // Log.e("html content"," hi   "+htmlcontent);
				       // txt.setTextSize(16);
				            txt.setText(Html.fromHtml(htmlcontent,null,new MyTagHandler()));
				            txt.setTextColor(Color.parseColor("#601261"));
				            txt.setTypeface(Typeface.SANS_SERIF);
				            break;
				        
			        	}
			        }
			      //  Log.d("i is  "," i is   "+k);
			      //  if(f==1){
			        	
			        
			        //  txt.setTypeface("Times New Roman");
			       // }
			        
			     //   else 
			      //  {
			        
			      //  }
		      
			
			} catch (StreamCorruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} /*catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/ catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

	}

}
